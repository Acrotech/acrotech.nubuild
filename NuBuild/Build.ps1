# Required Function Implementations
# =================================
# function OnError([string]$message, [int]$code) :: VOID
# function Log([string]$message) :: VOID

# Optional Environment Variables
# ==============================
# GitVersion_BranchName
# GitVersion_Sha
# BuildCounter
# GitVersion_PreReleaseTag
# GitVersion_BuildMetaData

function Log-Build([string]$message)
{
	Log "[NuBuild] $message"
}

function OnError-Exit([string]$message)
{
	if ($LASTEXITCODE -and $LASTEXITCODE -ne 0)
	{
		OnError "[!ERROR!] $message" $LASTEXITCODE
	}
}

function Get-ValueOrDefault([string]$value, [string]$defaultValue = "")
{
	if ([string]::IsNullOrEmpty($value)) { $defaultValue } else { $value }
}

function Get-FormatOrDefault([string]$format, [string]$value, [string]$defaultValue = "")
{
	if ([string]::IsNullOrEmpty($value)) { $defaultValue } else { [string]::Format($format, $value) }
}

function Initialize-Environment()
{
    if ($MyInvocation.MyCommand.Path)
    {
        $env:NuBuildRootPath = Get-ValueOrDefault $env:NuBuildRootPath (Split-Path -Path $MyInvocation.MyCommand.Path)
    }
    $env:SourcesPath = Get-ValueOrDefault $env:SourcesPath (Join-Path $env:NuBuildRootPath "..")
    $env:VisualStudioVersion = Get-ValueOrDefault $env:VisualStudioVersion "12.0"
    $env:NuGetOutDir = Get-ValueOrDefault $env:NuGetOutDir (Join-Path $env:NuBuildRootPath "nupkg")
    
    $env:MsBuildExe = Get-ValueOrDefault $env:MsBuildExe (Join-Path $env:SystemRoot "Microsoft.NET\Framework\v4.0.30319\MSBuild.exe")
    $env:TextTransformExe = Get-ValueOrDefault $env:TextTransformExe (Join-Path ${env:CommonProgramFiles(x86)} "microsoft shared\TextTemplating\$env:VisualStudioVersion\TextTransform.exe")
	$env:VsTestConsoleExe = Get-ValueOrDefault $env:VsTestConsoleExe (Join-Path ${env:ProgramFiles(x86)} "Microsoft Visual Studio $env:VisualStudioVersion\Common7\IDE\CommonExtensions\Microsoft\TestWindow\VsTest.Console.exe")
    $env:NuGetExe = Get-ValueOrDefault $env:NuGetExe (Join-Path $env:SourcesPath ".nuget\NuGet.exe")
    
    $env:Targets = Get-ValueOrDefault $env:Targets "Rebuild"
	$env:Configuration = Get-ValueOrDefault $env:Configuration "Release"
	$env:Platform = Get-ValueOrDefault $env:Platform "AnyCPU"
    
    if (Test-Path $env:NuGetOutDir)
	{
		Log-Build "Deleting Old NuGet packages ($env:NuGetOutDir\*.nupkg)"
		Remove-Item (Join-Path $env:NuGetOutDir "*.nupkg") -Recurse
	}
	else
	{
		Log-Build "Creating NuGet packages directory ($env:NuGetOutDir)"
		New-Item -ItemType Directory -Path $env:NuGetOutDir | Out-Null
	}
    
    Log-Build @"
NuBuild Environment:
NuBuildRootPath     = $env:NuBuildRootPath
SourcesPath         = $env:SourcesPath
VisualStudioVersion = $env:VisualStudioVersion
MsBuildExe          = $env:MsBuildExe
TextTransformExe    = $env:TextTransformExe
VsTestConsoleExe    = $env:VsTestConsoleExe
NuGetExe            = $env:NuGetExe
Targets             = $env:Targets
Configuration       = $env:Configuration
Platform            = $env:Platform
"@
}

function Prep-Project([string]$basepath)
{
	$path = Join-Path $basepath "Properties\build.properties"
	
	Log-Build "Prepping $path"

	[string]::Format("# Generated {0}", (Get-Date -Format u))								| Out-File -FilePath $path
	[string]::Format("Branch = {0}",	(Get-ValueOrDefault $env:GitVersion_BranchName))	| Out-File -FilePath $path -Append
	[string]::Format("Commit = {0}",	(Get-ValueOrDefault $env:GitVersion_Sha))			| Out-File -FilePath $path -Append
	[string]::Format("Number = {0}",	(Get-ValueOrDefault $env:BuildCounter))				| Out-File -FilePath $path -Append
	[string]::Format("Tag = {0}",		(Get-ValueOrDefault $env:GitVersion_PreReleaseTag))	| Out-File -FilePath $path -Append
	[string]::Format("MetaData = {0}",	(Get-ValueOrDefault $env:GitVersion_BuildMetaData))	| Out-File -FilePath $path -Append

	Get-Content $path
}

function Build([string]$basepath, [string]$project)
{
	Transform-Project-Version $basepath
    
    Build-Project (Join-Path $basepath $project)
}

function Transform-Project-Version([string]$basepath)
{
    $path = Join-Path $basepath "Properties\VersionDetails.tt"
    
	if (Test-Path $path)
	{
        Log-Build "Transforming $path"
        
        Log-Build "$env:TextTransformExe $path"
		& "$env:TextTransformExe" $path
		OnError-Exit "Transform Failed"
	}
}

function Build-Project([string]$projectPath)
{
    Log-Build "Building $projectPath"
    
    Log-Build "$env:MsBuildExe $projectPath /t:$env:Targets /p:Configuration=$env:Configuration /p:Platform=$env:Platform"
	& "$env:MsBuildExe" $projectPath /t:$env:Targets /p:Configuration=$env:Configuration /p:Platform=$env:Platform
	OnError-Exit "MSBuild Failed"
}

function Run-Tests([string]$assemblyPath)
{
	Log-Build "Testing $assemblyPath"

	& "$env:VsTestConsoleExe" $assemblyPath
	OnError-Exit "VsTest.Console Failed"
}

function Pack-Packagable([string]$packagablePath)
{
	Log-Build "Packing $packagablePath"

	$args = "Pack $packagablePath -NonInteractive -NoPackageAnalysis -OutputDirectory $env:NuGetOutDir"
	$args += Get-FormatOrDefault " -Verbosity {0}" $env:Verbosity
	$args += Get-FormatOrDefault " -Prop Configuration={0}" $env:Configuration
	$args += Get-FormatOrDefault " -Version {0}" $env:OverrideVersion

	Log-Build "$env:NuGetExe $args"
	Invoke-Expression "$env:NuGetExe $args"
	OnError-Exit "NuGet Pack Failed"
}

function NuBuild-Discover()
{
    $scripts = Get-ChildItem -Path $env:SourcesPath -Include @("*NuBuild.ps1", "*NuBuild.bat", "*NuBuild.cmd") -Recurse
    
    ForEach ($script in $scripts)
    {
        Log-Build "Running Build Script: $script"
        
        & $script
    }
}

function NuBuild-Inject()
{
    if (![string]::IsNullOrEmpty($env:BuildIncludes))
    {
        ForEach ($project in $env:BuildIncludes.Split(','))
        {
            Log-Build "Injecting Build: $project"
        
            Build-Project (Join-Path $env:SourcesPath $project)
        }
    }
    
    if (![string]::IsNullOrEmpty($env:TestIncludes))
    {
        ForEach ($test in $env:TestIncludes.Split(','))
        {
            Log-Build "Injecting Test: $test"
            
            Run-Tests (Join-Path $env:SourcesPath $test)
        }
    }
    
    if (![string]::IsNullOrEmpty($env:PackIncludes))
    {
        ForEach ($packagable in $env:PackIncludes.Split(','))
        {
            Log-Build "Injecting Pack: $packagable"
            
            Pack-Packagable (Join-Path $env:SourcesPath $packagable)
        }
    }
}

# initialize required environment variables
Initialize-Environment

#Get-ChildItem Env: | Sort-Object Name | Select-Object Name, Value

# check if we should force using the GitVersion_NuGetVersion value
if (![string]::IsNullOrEmpty($env:ForceGitVersionOverride))
{
	$env:OverrideVersion = $env:GitVersion_NuGetVersion
	Log-Build "Overriding Version with NuGet Version: $env:OverrideVersion"
}

# manual build statements (DISCOURAGED!!! use a NuBuild.ps1 or NuBuild.bat file instead)
# Prep-Project (Join-Path $env:SourcesPath "My.Project")
# Build-Project (Join-Path $env:SourcesPath My.Project) "My.Project.csproj"
# Run-Tests (Join-Path $env:SourcesPath "My.Project.Test\bin\$env:Configuration\My.Project.Test.dll")
# Pack-Packagable (Join-Path $env:SourcesPath "My.Project\My.Project.nuspec")

# call any build scripts discovered within the SourcesPath
NuBuild-Discover

# call any build functions with injected variables
NuBuild-Inject

Log-Build "Complete"
