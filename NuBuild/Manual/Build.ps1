function Log([string]$message)
{
	Write-Host $message
}

function OnError([string]$message, [int]$code)
{
	Write-Host "$message ($code)"
	exit $code
}

Log "[NuBuild] Manual Build Started..."

# $env:GitVersion_BranchName = ""
# $env:GitVersion_BranchName = ""
# $env:GitVersion_Sha = ""
# $env:BuildCounter = ""
# $env:GitVersion_PreReleaseTag = ""
# $env:GitVersion_BuildMetaData = ""

# $env:OverrideVersion = "1.2.3"

$env:NuBuildRootPath = (Join-Path (Split-Path -Path $MyInvocation.MyCommand.Path) "..")

Log "[NuBuild] Setting Location to $env:NuBuildRootPath"
Set-Location $env:NuBuildRootPath

.\Build.ps1
