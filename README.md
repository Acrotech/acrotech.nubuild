﻿# README #

## Status ##

| Branch  | MyGet Build | MyGet Feed | MyGet Downloads | NuGet Feed | NuGet Downloads |
|---------|-------------|------------|-----------------|------------|-----------------|
| develop | [![MyGet Build](https://www.myget.org/BuildSource/Badge/acrotech-develop?identifier=17546033-f1bd-489d-acee-58d130f02a1b)](https://www.myget.org/gallery/acrotech-develop) | [![MyGet Feed](https://img.shields.io/myget/acrotech-develop/vpre/Acrotech.NuBuild.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-develop) | [![MyGet Downloads](https://img.shields.io/myget/acrotech-develop/dt/Acrotech.NuBuild.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-develop) | [![NuGet Feed](https://img.shields.io/badge/nuget-N%2FA-lightgrey.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.NuBuild) | [![NuGet Downloads](https://img.shields.io/badge/nuget-N%2FA-lightgrey.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.NuBuild) |
| beta    | [![MyGet Build](https://www.myget.org/BuildSource/Badge/acrotech-beta?identifier=b6ee73ca-350c-40c2-a7d6-906c646833cb)](https://www.myget.org/gallery/acrotech-beta) | [![MyGet Feed](https://img.shields.io/myget/acrotech-beta/vpre/Acrotech.NuBuild.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-beta) | [![MyGet Downloads](https://img.shields.io/myget/acrotech-beta/dt/Acrotech.NuBuild.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-beta) | [![NuGet Feed](https://img.shields.io/nuget/vpre/Acrotech.NuBuild.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.NuBuild) | [![NuGet Downloads](https://img.shields.io/badge/nuget-N%2FA-lightgrey.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.NuBuild) |
| master  | [![MyGet Build](https://www.myget.org/BuildSource/Badge/acrotech?identifier=a867fc10-5177-4f17-876b-909283316b91)](https://www.myget.org/gallery/acrotech) | [![MyGet Feed](https://img.shields.io/myget/acrotech/v/Acrotech.NuBuild.svg?style=flat-square)](https://www.myget.org/gallery/acrotech) | [![MyGet Downloads](https://img.shields.io/myget/acrotech/dt/Acrotech.NuBuild.svg?style=flat-square)](https://www.myget.org/gallery/acrotech) | [![NuGet Feed](https://img.shields.io/nuget/v/Acrotech.NuBuild.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.NuBuild) | [![NuGet Downloads](https://img.shields.io/nuget/dt/Acrotech.NuBuild.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.NuBuild) |


## Instructions ##

## Changelog ##

#### 1.0.0 ####

* Initial Release
